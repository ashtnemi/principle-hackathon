from django.http import HttpResponse
from .models import user
from .models import coursesByInstructor
from django.template import loader
from django.shortcuts import render

uname=""

def index(request):
    return render(request, 'loginNregister/index.html')

def register(request):
    return render(request, 'loginNregister/register.html')

def api(request):
    return render(request, 'loginNregister/api.html')

def success(request):
    return render(request, 'loginNregister/success.html')

def coursesInfo(request):
    all = coursesByInstructor.objects.all()
    nm=request.session['username']
    print(nm)
    c=[]
    for itr in all:
        if(itr.username==nm):
            c.append(itr.course)

    context = {
        'c':c,
    }

    for itr in c:
        print(itr+'\n')

    template = loader.get_template('loginNregister/coursesInfo.html')
    return HttpResponse(template.render(context, request))

def loginAction(request):
    itr=""
    nm = request.POST['username']
    ps = request.POST['password']
    ty= request.POST['group3']
    request.session['username'] = nm
    uname=nm

    all=user.objects.all()
    for itr in all:
        if(itr.username==nm and itr.password==ps):

            if(ty=="student"):
                return render(request, 'loginNregister/loggedInIndexStu.html')

            if (ty == "instructor"):
                return render(request, 'loginNregister/loggedInIndexIns.html')

            if (ty == "admin"):
                return render(request, 'loginNregister/loggedInIndexAdmin.html')

    return render(request, 'loginNregister/fail.html')

def loggedInIndexStu(request):
    return render(request, 'loginNregister/loggedInIndexStu.html')

def loggedInIndexIns(request):
    return render(request, 'loginNregister/loggedInIndexIns.html')

def loggedInIndexAdmin(request):
    return render(request, 'loginNregister/loggedInIndexAdmin.html')


def registerAction1(request):
    # return HttpResponse("<h1>hello</h1>")
    un = request.POST['username']
    pw = request.POST['password']
    em = request.POST['email']
    ty=request.POST['type']

    print("zala")

    userObj=user()
    userObj.username=un
    userObj.password=pw
    userObj.email=em
    userObj.email = ty

    userObj.save()

    return render(request, 'loginNregister/index.html')


