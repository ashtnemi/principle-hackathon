from django.conf.urls import include,url
from django.contrib import admin
from . import views

urlpatterns=[
    url(r'^$',views.index,name="ïndex"),
    #url(r'^infoo/',views.infoo,name="infoo"),
    url(r'^$',views.index,name="ïndex"),
    url(r'^register/', views.register, name="register"),
    url(r'^registerAction1/', views.registerAction1, name="registerAction1"),
    url(r'^loginAction/', views.loginAction, name="loginAction"),
    url(r'^api/$', views.api, name="api"),
    url(r'^loggedInIndexStu/', views.loggedInIndexStu, name="loggedInIndexStu"),
    url(r'^loggedInIndexIns/', views.loggedInIndexIns, name="loggedInIndexIns"),
    url(r'^loggedInIndexAdmin/', views.loggedInIndexAdmin, name="loggedInIndexAdmin"),
    url(r'^coursesInfo/', views.coursesInfo, name="coursesInfo"),

    # url(r'^enrollRequest/', views.enrollRequest, name="enrollRequest"),
]
