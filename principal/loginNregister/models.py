from django.db import models

class user(models.Model):
    username=models.CharField(max_length=250)
    password=models.CharField(max_length=250)
    email=models.CharField(max_length=250)
    type=models.CharField(max_length=250)
    couseRegistered=models.CharField(max_length=250)

    def __str__(self):
        return self.username+'-'+self.password+'-'+self.email+'-' +self.type+'-' +self.couseRegistered


class coursesByInstructor(models.Model):
    username = models.CharField(max_length=250)
    course = models.CharField(max_length=250)
    studentsEnrolled=models.CharField(max_length=250)

    def __str__(self):
        return self.username+'-'+self.course